package com.rahasak.circe.protocol

import cats.syntax.either._
import io.circe.{Decoder, Encoder, HCursor, Json}

case class Account(id: String, name: String, isActive: Boolean, createTime: Long)

object Account {
  implicit val encoderAccount = new Encoder[Account]() {
    final def apply(a: Account): Json = Json.obj(
      ("id", Json.fromString(a.id)),
      ("name", Json.fromString(a.name)),
      ("is_active", Json.fromBoolean(a.isActive)),
      ("create_time", Json.fromLong(a.createTime))
    )
  }

  implicit val decodeAccount = new Decoder[Account] {
    final def apply(c: HCursor): Decoder.Result[Account] = {
      for {
        id <- c.downField("id").as[String]
        name <- c.downField("name").as[String]
        isActive <- c.downField("is_active").as[Boolean]
        createTime <- c.downField("create_time").as[Long]
      } yield {
        new Account(id, name, isActive, createTime)
      }
    }
  }
}


