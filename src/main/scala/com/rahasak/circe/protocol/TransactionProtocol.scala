package com.rahasak.circe.protocol

import enumeratum._

sealed trait TransactionType extends EnumEntry

case object TransactionType extends CirceEnum[TransactionType] with Enum[TransactionType] {

  case object Debit extends TransactionType

  case object Credit extends TransactionType

  val values = findValues

}

case class Bank(code: String, name: String)

case class Transaction(id: String, transactionType: TransactionType, bank: Option[Bank])
