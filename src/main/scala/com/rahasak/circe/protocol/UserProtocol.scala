package com.rahasak.circe.protocol

case class User(id: String, name: String, email: Option[String], isActive: Boolean)

