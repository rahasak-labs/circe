package com.rahasak.circe.protocol

case class Member(name: String, email: String, department: Option[String])
