package com.rahasak.circe.protocol

case class Meta(offset: Int, limit: Int, total: Int)

case class Token(id: String, name: String, value: String, desc: Option[String])

case class TokenResponse(meta: Meta, tokens: List[Token])

