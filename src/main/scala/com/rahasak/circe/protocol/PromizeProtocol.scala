package com.rahasak.circe.protocol

case class Creator(id: String, name: String, email: String)

case class Promize(id: String, creator: Creator, createTime: Long)
