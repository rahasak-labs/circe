package com.rahasak.circe.protocol

import io.circe.generic.extras.semiauto._

sealed trait DocumentType

case object Invoice extends DocumentType

case object Order extends DocumentType

case class Company(id: String, name: String)

case class Document(id: String, name: String, documentType: DocumentType, company: Option[Company] = None,
                    approved: Option[Boolean] = None)

object Document {
  implicit val encoderDocument = deriveEnumerationEncoder[DocumentType]
  implicit val decoderDocument = deriveEnumerationDecoder[DocumentType]
}

