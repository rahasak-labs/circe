package com.rahasak.circe

object Main extends App {

  def handleUser(): Unit = {
    import com.rahasak.circe.protocol.User
    import io.circe._
    import io.circe.generic.auto._
    import io.circe.syntax._
    val usr = User("9112", "rahasak", None, isActive = false)
    println(usr.asJson.noSpaces)

    val json =
      """
        |{"id":"9112","name":"rahasak","email":null,"isActive":false}
      """.stripMargin
    println(parser.decode[User](json))
  }

  def handlePromize(): Unit = {
    import com.rahasak.circe.protocol.{Creator, Promize}
    import io.circe._
    import io.circe.generic.auto._
    import io.circe.syntax._

    val usr = Promize("41112", Creator("42323", "rahasak", "ops@rahasak.io"), System.currentTimeMillis() / 1000)
    println(usr.asJson.noSpaces)

    val json =
      """
        |{"id":"41112","creator":{"id":"42323","name":"rahasak","email":"ops@rahasak.io"},"createTime":1571788807}
      """.stripMargin
    println(parser.decode[Promize](json))
  }

  def handleToken(): Unit = {
    import com.rahasak.circe.protocol.{Meta, Token, TokenResponse}
    import io.circe._
    import io.circe.generic.auto._
    import io.circe.syntax._

    val meta = Meta(0, 10, 2341)
    val t1 = Token("01", "ops", "212112", Option("token 1"))
    val t2 = Token("02", "lambda", "321211", None)
    val t3 = Token("03", "rahasak", "421221", None)
    val resp = TokenResponse(meta, List(t1, t2, t3))
    println(resp.asJson.noSpaces)

    val json =
      """
        |{
        |  "meta": {
        |    "offset": 0,
        |    "limit": 10,
        |    "total": 2341
        |  },
        |  "tokens": [
        |    {
        |      "id": "01",
        |      "name": "ops",
        |      "value": "212112",
        |      "desc": "token 1"
        |    },
        |    {
        |      "id": "02",
        |      "name": "lambda",
        |      "value": "321211",
        |      "desc": null
        |    },
        |    {
        |      "id": "03",
        |      "name": "rahasak",
        |      "value": "421221",
        |      "desc": null
        |    }
        |  ]
        |}
      """.stripMargin
    println(parser.decode[TokenResponse](json))
  }

  def handleAccount(): Unit = {
    import com.rahasak.circe.protocol.Account
    import Account._
    import io.circe._
    import io.circe.syntax._
    val acc = Account("001", "ops", isActive = false, System.currentTimeMillis() / 1000)
    println(acc.asJson.noSpaces)

    val json =
      """
        |{"id":"001","name":"ops","is_active":false,"create_time":1571782742}
      """.stripMargin
    println(parser.decode[Account](json))
  }

  def handleTransaction(): Unit = {
    import com.rahasak.circe.protocol.{Bank, Transaction, TransactionType}
    import io.circe._
    import io.circe.generic.auto._
    import io.circe.syntax._
    val trans = Transaction("011", TransactionType.Debit, Option(Bank("311", "sampath")))
    println(trans.asJson.noSpaces)

    val json =
      """
        |{
        |  "id": "011",
        |  "transactionType": "Debit",
        |  "bank": {
        |    "code": "311",
        |    "name": "sampath"
        |  }
        |}
      """.stripMargin
    println(parser.decode[Transaction](json))
  }

  def handleDocument(): Unit = {
    import com.rahasak.circe.protocol.{Company, Document, Invoice}
    import Document._
    import io.circe._
    import io.circe.generic.auto._
    import io.circe.syntax._

    val doc = Document("211", "rahasak doc", Invoice, Option(Company("3232", "rahasak")))
    println(doc.asJson.noSpaces)

    val json =
      """
        |{"id":"211","name":"rahasak doc","documentType":"Invoice","company":{"id":"3232","name":"rahasak"},"approved":null}
      """.stripMargin
    println(parser.decode[Document](json))
  }

  def handleMember(): Unit = {
    import cats.syntax.either._
    import com.rahasak.circe.protocol.Member
    import io.circe._
    import io.circe.generic.auto._

    val json =
      """
        |{
        |  "name": "Group A",
        |  "company": {
        |    "name": "rahasak",
        |    "branch": {
        |      "name": "dev",
        |      "code": "42232"
        |    },
        |    "count": 4223
        |  },
        |  "members": [
        |    {
        |      "name": "eranga",
        |      "email": "eranga@rahasak.io",
        |      "department": null
        |    },
        |    {
        |      "name": "herath",
        |      "email": "herath@rahasak.io",
        |      "department": "dev"
        |    }
        |  ]
        |}
      """.stripMargin

    val branch = parser.parse(json).getOrElse(Json.Null)
      .hcursor
      .downField("company")
      .downField("branch")
      .get[String]("name")
      .toOption
    println(branch)

    val members = parser.parse(json).getOrElse(Json.Null)
      .hcursor
      .downField("members")
      .focus
      .flatMap(_.asArray)
      .getOrElse(Vector.empty)
      //.flatMap(_.hcursor.downField("email").as[String].toOption)
      .flatMap(_.as[Member].toOption)
    println(members)
  }

  handleUser()
  handlePromize()
  handleToken()
  handleAccount()
  handleTransaction()
  handleDocument()
  handleMember()

}

